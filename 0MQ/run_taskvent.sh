#!/bin/sh

#Since this file is COPYed to the container, any changes require the docker image to be destroyed
#docker rmi 0mq_vent
#so changes are reflected to the container!!!

echo "Starting sending out jobs after loading:"
PERCENTAGE="0"
while [ ${PERCENTAGE} -lt 101 ]; do
    /bin/echo -n "....${PERCENTAGE}%"
    PERCENTAGE=`expr ${PERCENTAGE} + 33`
    sleep 1
done
echo 'Starting taskvent.py...'
python /tmp/taskvent.py
