Repository containing the original Python examples of the [ZeroMQ guide](http://zguide.zeromq.org/page:all) adapted to Docker.

So instead of running each example manually, it's possible to run them with `docker-compose up`.


So far only the first book example with a Ventilator, some Workers and a Sink has been created.

Run it by entering the 0MQ folder and running:

`docker-compose up --scale work=2`

to have 2 workers (for example)!


A presto,
iltomasi

